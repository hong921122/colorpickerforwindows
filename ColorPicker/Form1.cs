﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace ColorPicker
{
    public partial class Form1 : Form
    {
        Bitmap bmpScreenshot;
        Graphics gfxScreenshot;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            MouseHook hook = new MouseHook();
            hook.OnMouseActivity += hook_OnMouseActivity;
        }

        void hook_OnMouseActivity(object sender, MouseEventArgs e)
        {
            pictureBox1.ImageLocation = "Screenshot.png";

            using (Bitmap bmpScreenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb))
            {
                using (Graphics g = Graphics.FromImage(bmpScreenCapture))
                {
                    g.CopyFromScreen(e.X,
                                     e.Y,
                                     100, 100,
                                     new Size(pictureBox1.Width, pictureBox1.Height),
                                     CopyPixelOperation.SourceCopy);
                    
                }
                bmpScreenCapture.Save("Screenshot.png", ImageFormat.Png);
            }
            //bmpScreenshot = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
            //                               Screen.PrimaryScreen.Bounds.Height,
            //                               PixelFormat.Format32bppArgb);

            //gfxScreenshot = Graphics.FromImage(bmpScreenshot);
            //gfxScreenshot.CopyFromScreen(e.X,
            //                            e.Y,
            //                            100,
            //                            100,
            //                            new Size(pictureBox1.Width, pictureBox1.Height),
            //                            CopyPixelOperation.SourceCopy);

            //pictureBox1.Image = bmpScreenshot;
            
            ////bmpScreenshot.Dispose();
            //bmpScreenshot.Save("Screenshot.png", ImageFormat.Png);
            richTextBox1.AppendText(e.X.ToString() + "//" + e.Y.ToString() + "\n");
        }
    }
}
